package com.gemini_is.utils.jwt.auth0;

import com.auth0.jwk.*;
import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTDecodeException;
import com.auth0.jwt.exceptions.TokenExpiredException;
import com.auth0.jwt.interfaces.Claim;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.auth0.jwt.interfaces.Verification;
import com.gemini_is.utils.SystemUtils;

import java.net.MalformedURLException;
import java.net.URL;
import java.security.interfaces.RSAPublicKey;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Copyright 2020 (c) Diego Alberto Huerta Silva
 *
 * @author Diego Alberto Huerta Silva
 * @version 1.0.0
 *
 * This is an utility class for processing JWT (parsing, creating, etc)
 */
public class JwtUtils
{
  public static String getUsername(String username) {
    return getUsername(username, SystemUtils.getVariable("JWK_PROVIDER_URL"));
  }
  public static String getUsername(String username, String jwkProviderUrl)
  {
    return getClaim(username, "username", jwkProviderUrl).asString();
  }
  public static String getSub(String token) {
    return getSub(token, SystemUtils.getVariable("JWK_PROVIDER_URL"));
  }
  public static String getSub(String token, String jwkProviderUrl)
  {
    return getClaim(token, "sub", jwkProviderUrl).asString();
  }

  public static Claim getClaim(String token, String claimName) {
    return getClaim(token, claimName, SystemUtils.getVariable("JWK_PROVIDER_URL"));
  }
  public static Claim getClaim(String token, String claimName, String jwkProviderUrl)
  {
    try {
      return getDecodedJwt(token, jwkProviderUrl).getClaim(claimName);
    } catch (Exception error) {
      throw new RuntimeException(error);
    }
  }

  /**
   * Checks if the given JWT in the parameter is valid an isn't expired.
   *
   * @param token JWT token
   * @return true|false depending if the token is valid or not
   */
  public static boolean checkIsValid(String token) {
    String jwkProviderUrl = SystemUtils.getVariable("JWK_PROVIDER_URL");
    return checkIsValid(token, jwkProviderUrl);
  }

  /**
   * Checks if the given JWT in the parameter is valid an isn't expired.
   *
   * @param token JWT token
   * @return true|false depending if the token is valid or not
   */
  public static boolean checkIsValid(String token, String jwkProviderUrl) {
    try {
      getDecodedJwt(token, jwkProviderUrl);
      return true;
    } catch (TokenExpiredException | SigningKeyNotFoundException | JWTDecodeException error) {
      return false;
    } catch (MalformedURLException | JwkException error) {
      throw new RuntimeException(error);
    }
  }

  /**
   * Check if the token is from an admin (admins have "admin", "group_admin" or "root" in "cognito:groups")
   * @param token Is the JWT token
   * @return true | false depending if the token is from an admin or not
   */
  public static boolean checkIsAdmin(String token) {
    return checkIsInGroups(token,"admin") || checkIsInGroups(token, "group_admin") ||
      checkIsInGroups(token, "root");
  }
  /**
   * Check if the token is from an admin (admins have "admin", "group_admin" or "root" in "cognito:groups")
   * @param token Is the JWT token
   * @return true | false depending if the token is from an admin or not
   */
  public static boolean checkIsAdmin(String token, String jwkProviderUrl) {
    return checkIsInGroups(token, jwkProviderUrl, Collections.singletonList("admin")) ||
      checkIsInGroups(token, jwkProviderUrl, Collections.singletonList("group_admin")) ||
      checkIsInGroups(token, jwkProviderUrl, Collections.singletonList("root"));
  }

  /**
   * Checks if the user have the root|admin role
   * @param token as a JWT token
   * @return true|false depending if the user is root or not.
   */
  public static boolean checkIsInGroups(String token, String... groups) {
    String jwkProviderUrl = SystemUtils.getVariable("JWK_PROVIDER_URL");
    return checkIsInGroups(token, jwkProviderUrl, Arrays.asList(groups));
  }

  /**
   * Checks if the user have the root|admin role
   * @param token as a JWT token
   * @return true|false depending if the user is root or not.
   */
  public static boolean checkIsInGroups(String token, String jwkProviderUrl, List<String> groups) {
    try {
      List<String> userGroups = getDecodedJwt(token, jwkProviderUrl).getClaim("cognito:groups").asList(String.class);

      return userGroups != null && (userGroups.containsAll(groups) || userGroups.contains("root"));

    } catch (JwkException | JWTDecodeException error) {
      return false;
    } catch (MalformedURLException error) {
      throw new RuntimeException(error);
    }
  }


  private static DecodedJWT getDecodedJwt(String token)
    throws JwkException, MalformedURLException {
    String jwkProviderUrl = SystemUtils.getVariable("JWK_PROVIDER_URL");
    return getDecodedJwt(token, jwkProviderUrl);
  }

  private static DecodedJWT getDecodedJwt(String token, String jwkProviderUrl)
    throws JwkException, MalformedURLException {
    String jwtKid = JWT.decode(token).getKeyId();

    JwkProvider provider = new UrlJwkProvider(
      new URL(jwkProviderUrl)
    );
    Jwk jwk = provider.get(jwtKid);
    Algorithm algorithm =  Algorithm.RSA256((RSAPublicKey)jwk.getPublicKey(), null);
    Verification verifier = JWT.require(algorithm).acceptLeeway(5).acceptExpiresAt(5);

    return verifier.build().verify(token);
  }
}
